sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "ZMM_PARMA_MRP_ABC/model/formatter",
        "ZMM_PARMA_MRP_ABC/model/models",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
        "sap/m/MessageBox"
    ],
    function (Controller, History, UIComponent, formatter, models, Filter, FilterOperator, MessageBox) {
        "use strict";

        return Controller.extend("ZMM_PARMA_MRP_ABC.controller.BaseController", {
            formatter: formatter,

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },
            onOpenDialog: function(oEvent  , sDialogName){
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("ZMM_PARMA_MRP_ABC.view.fragments." + sDialogName +'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onCloseDialog: function (sDialogName) {
                this[sDialogName].close();
            },
            onPressSecAddrDialog: function(oEvent, sDialogName){
                var index = oEvent.getSource().getBindingContext("JSON").getPath().split("/")[2];
                oComponent_ManagerCreate.getModel("JSON").setProperty("/IndexRow" , index);
                this.onOpenDialog('' ,sDialogName);
            },
            onValueHelpClose: function (oEvent , sType) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                oEvent.getSource().getBinding("items").filter([]);
    
                if (!oSelectedItem) {
                    return;
                }
                else if(sType === 'Kostl'){
                    var skostl = oSelectedItem.getProperty("description");
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/kostlKey" , skostl);
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/kostlInput" , oSelectedItem.getTitle());
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/mainAdrrKey" , "");
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrKey" , "");
                    
                    models.LoadMainAdrrSh(skostl).then(function(data) {
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/MainAdrrSh" , data.results);
                        if(data.results.length === 1){
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/mainAdrrKey" , data.results[0].Adrnr);
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/MainAddrInput" , data.results[0].Adname);
                            
                            models.LoadSecAddrSh(data.results[0].Adrnr).then(function(data) {
                                oComponent_ManagerCreate.getModel("JSON").setProperty("/SecAddrSh" , data.results);
                                if(data.results.length === 1){
                                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrKey" , data.results[0].Adrnr);
                                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrInput" , data.results[0].Adname); 
                                              
                                }
                            }).catch(function (error) {
                                console.log(error)
                            });
                        }
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
                
            },
            onChangeAddr: function (oEvent , sType) {
                var selectedItem = oEvent.getParameter("selectedItem").getBindingContext("JSON").getObject(),
                    sAddr = selectedItem.Adrnr,
                    sAddrName = selectedItem.Adname;              

                if(sType === 'MainAddr'){
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/MainAddrInput" , sAddrName);
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrKey" , "");
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrInput" , "");
                    models.LoadSecAddrSh(sAddr).then(function(data) {
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/SecAddrSh" , data.results);
                        if(data.results.length === 1){
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrKey" , data.results[0].Adrnr);       
                            oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrInput" , data.results[0].Adname);              
                        }
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
                else if (sType === 'secAddr'){
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/secAddrInput" , sAddrName);
                }
                else{
                    var oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                    oRow.SecondaryAddressName = sAddrName;
                    oComponent_ManagerCreate.getModel("JSON").refresh();
                }
            },
            onValueHelpSearch: function (oEvent , sType) {
                var sValue = oEvent.getParameter("value");
                var oFilter = [];
                if(sType === 'Kostl'){
                     oFilter.push(new Filter("Name", FilterOperator.Contains, sValue));
                    
                }
                else if(sType ==='Order'){
                    oFilter.push(new Filter("Order", FilterOperator.Contains, sValue));
                }
                else{
                     oFilter = new Filter({
                        filters: [new Filter("Adname", FilterOperator.Contains, sValue),
                            new Filter("Adrnr", FilterOperator.Contains, sValue)
                        ],
                        and: false
                    });
                }  
                // var oList = this.byId("kostlList");
                // var oBinding = oList.getBinding("items");
                // oBinding.filter(aFilters, "Application");       
                var oBinding = oEvent.getParameter("itemsBinding");
			    oBinding.filter(oFilter);
                // oEvent.getSource().getItems().filter(oFilter);
            },
            /**
             * Convenience method for setting the view model in every controller of the application.
             * @public
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} sName the model name
             * @returns {sap.ui.mvc.View} the view instance
             */
            setModel: function (oModel, sName) {
                return this.getView().setModel(oModel, sName);
            },

            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            getResourceBundle: function () {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle();
            },

            /**
             * Method for navigation to specific view
             * @public
             * @param {string} psTarget Parameter containing the string for the target navigation
             * @param {Object.<string, string>} pmParameters? Parameters for navigation
             * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
             */
            navTo: function (oEvent, sPage) {
                this.getRouter().navTo(sPage);
            },

            getRouter: function () {
                return UIComponent.getRouterFor(this);
            },
            onPressComment: function(oEvent){
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                oComponent_ManagerCreate.getModel("JSON").setProperty("/oSelectedRow" ,oSelectedItem);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/newItemText" ,oSelectedItem.ItemText);
                this.onOpenDialog('','comment');
            },
            saveCommentRow: function(oEvent){
                var oModel = oComponent_ManagerCreate.getModel("JSON"),
                    oRow = oModel.getProperty("/oSelectedRow");
                    oRow.ItemText = oModel.getProperty("/newItemText");
                    oComponent_ManagerCreate.getModel("JSON").refresh();
                    this.onCloseDialog('comment');
            },
            createNewOrder: function(){

            },
            onCreateOrder: function(type , items){
                var oModel = oComponent_ManagerCreate.getModel("JSON").getData(),
                    aItems = [];
                for(var i = 0; i<items.length; i++){
                    aItems.push({
                        Agrmnt:    items[i].Agrmnt,               
                        Item: items[i].Item,
                        AgrmntItem: items[i].AgrmntItem,                     
                        // Matnr: '50000020',
                        Matnr: items[i].Matnr,
                        Quantity: (items[i].OrderQun).toString(),
                        Unit: items[i].OrderUnit,
                        UnitPrice: items[i].UnitPrice,
                        ItemAdrnr: items[i].SecondaryAddress,
                        ItemText: items[i].ItemText
                    })
                }
                var oEntry = {
                    EvOutput: '',
                    IvActivity : type,
                    IvDeliveryDate : new Date(),
                    IvCostCenter:  oModel.kostlKey,
                    IvLogisticCenter: oModel.Vendor === '1000',
                    IvOrderNum : oModel.OrderNumber || '',
                    IvOrderText : !!oModel.orderText ? oModel.orderText :'',
                    IvPriceOutput : oModel.PriceOutput || false,
                    IvPurGroup : oModel.Ekgrp,
                    IvPurOrg : oModel.EkorgSearch || '',
                    IvVendor : oModel.Vendor === '1000' ? '' : oModel.Vendor,
                    OrderItems : aItems,
                    EvOrderNum: '',
                    Return : []
                }
            
                models.createOrder(oEntry).then(function(data) {
                    oComponent_ManagerCreate.getModel("JSON").setProperty("/OrderNumber" ,data.EvOrderNum);
                    if(data.Return.results.length > 0){
                        this.showErrorsMsg(data.Return.results);
                    }  
                    else if(type === '1'){
                        oComponent_ManagerCreate.getModel("JSON").setProperty("/orderItems" ,oModel.approvalItems);
                        this.navTo('',"Review");    
                    }
                         
                   else if(type === '2'){
                    this.onOpenDialog("" , 'saveDefect');
                   }
                   else if(type === '3'){
                    this.onOpenDialog("" , 'createOrder');
                   }
                   else if(type === '4'){
                    this.onOpenDialog("" , 'saveDefect');
                   }
                   else if(type === '5'){
                    this.onOpenDialog("" , 'deleteOrder');
                   }
                   
                }.bind(this)).catch(function (error) {
                    console.log(error);
                    try {
                        MessageBox.error(JSON.parse(error.responseText).error.message.value);
                    } catch (e) {
                        MessageBox.error(JSON.stringify(error));
                    }
                });
            },
            showErrorsMsg: function(aErrors){
                oComponent_ManagerCreate.getModel("JSON").setProperty("/Errors" ,aErrors);
                this.onOpenMessageViewDialog();

            },

            onConfirm: function(oEvent){
                var row = oEvent.getParameter("selectedItem").getBindingContext("ODATA").getObject();
                oComponent_ManagerCreate.getModel("JSON").setProperty("/VendorFromOrder" , row.Vendor);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/VendorName" , row.VendorName);
                oComponent_ManagerCreate.getModel("JSON").setProperty("/OrderNumberCopy", '');
                oComponent_ManagerCreate.getModel("JSON").setProperty("/OrderNumberCopy" , row.Order);

            },

            onNavBack: function () {
                var  oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({ 
                    target: { shellHash: "#" } 
                    });
             
            },
            GoToMM03: function (event , Mat) {
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    var semamticActionObj = "MM03-display";
                    var oParams = {
                        MAT: Mat
                    };
                    oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                    if (aResponses[semamticActionObj].supported === true) {
                    var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                    target: {
                    semanticObject: "MM03",
                    action: "display"
                    },
                    params: oParams
                    })) || "";
                    oCrossAppNavigator.toExternal({
                    target: {
                    shellHash: hash
                    }
                    });
                    }
                    
                    })
                    .fail(function () {});
                },
                goToReport: function(oEvent){
                    var Matnr = oEvent.getSource().getBindingContext("JSON").getObject().Matnr;
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    var semamticActionObj = "zmm_manager_report-display";
                    var oParams = {
                        Matnr: Matnr
                    };
                    oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                    if (aResponses[semamticActionObj].supported === true) {
                    var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                    target: {
                    semanticObject: "zmm_manager_report",
                    action: "display"
                    },
                    params: oParams
                    })) || "";
                    sap.m.URLHelper.redirect(hash,true);
                    }
                    
                    })
                    .fail(function () {});
                    
                },
                openOutput : function(sOrderNum){
                    models.LoadOutput(sOrderNum).then(function (data) {
                        var byteCharacters = atob(data.EvOutput);
                        var byteNumbers = new Array(byteCharacters.length);
                        for (var i = 0; i < byteCharacters.length; i++) {
                            byteNumbers[i] = byteCharacters.charCodeAt(i);
                        }
                        var byteArray = new Uint8Array(byteNumbers);
                        if(window.navigator.msSaveOrOpenBlob) {
                            var blob = new Blob([byteArray], {type: 'application/pdf'});
                            window.navigator.msSaveOrOpenBlob(blob, "Draft" +".pdf");
                        }
                        else{
                        var file = new Blob([byteArray], { type: 'application/pdf;base64' });
                        var fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                        }
                    }).catch(function (error) {
                                console.log(error)
                    });
                   
                 },
                 onOpenMessageViewDialog: function(oEvent){
                    if (!oComponent_ManagerCreate._MessageView) {
                        oComponent_ManagerCreate._MessageView = sap.ui.xmlfragment("ZMM_PARMA_MRP_ABC.view.fragments." + 'MessageViewDialog', this);
                        this.getView().addDependent(oComponent_ManagerCreate._MessageView);
                    }
                    oComponent_ManagerCreate._MessageView.open();
                },
                onCloseMessageViewDialog: function (oEvent) {
                    oComponent_ManagerCreate._MessageView.close();
                },
                 afterMessageViewClose: function(oEvent){
                    oComponent_ManagerCreate._MessageView.destroy();
                    oComponent_ManagerCreate._MessageView = undefined;
                    

                 }
        });
    });
